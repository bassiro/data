#!/usr/bin/env python
# coding: utf-8
from __future__ import (print_function, division,
    absolute_import, unicode_literals)
import sys
import warnings
import pandas as pd 
import numpy as np
import matplotlib
import os
import json


def construction_data(path):    
    f = open(path+'.tmp','r')
    data=f.read()
    f.close() 
    tx=data.split('=')[1].split('},')
    dataf={}
    text=""
    textdata=''
    list_color=['#17BECF','#C2185B','#1A237E','#651fff','#d50000','#00c853','#ffff00','#dd2c00','#212121','#000000','#ef9a9a','#ce93d8','#9fa8da','#81d4fa','#01579b','#33691e','#64dd17','#5d4037','#bcaaa4','#aa00ff','#ec407a']
    for k in range(len(tx)):
        x=[]
        y=[]
        name=''
        for el in tx[k].split("'x':")[1].split("'y':")[0].replace('],','').split(','):
                x.append(el.split('name')[0].replace('[','').replace("'","").replace(" ",''))
        for el in tx[k].split("'y':")[1].split("'x':")[0].split('name')[0].replace('],','').split(','):
                try:
                    y.append(int(el.replace('[','').replace(' ','').replace("'",'')))
                except:
                    y.append(float(el.replace('[','').replace(' ','').replace("'",'')))
                    pass
        name=tx[k].split("'x':")[1].split("'y':")[0].split('name')[1].replace("':'",'').replace('}','').replace(":",'').replace("'",'').replace(" ",'')
        dataf[k+1]={'x':x,'y':y,'name':name}
        text=text+"""  
        var trace"""+str(k+1)+"""= {
        type: "scatter",
        mode: "lines",
          name: '"""+str(name)+"""',
          x: """+str(x).replace("u'","'")+""",
          y: """+str(y)+""",
          line: {color: '"""+list_color[k%21]+"""'}
        } \n
        """
        textdata=textdata+'trace'+str(k+1)+','
    string="""
     <head>
    	<!-- Load plotly.js into the DOM -->
    	<script src='../plotly-latest.min.js'></script>
    </head>
    <body>
    
    <div id="myDiv"> </div>
    </body>
    <script>
    
    """
    string=string+text
    string=string+"""
    var data = ["""+textdata[:-1]+"""];
    
    var layout = {
    };
    Plotly.newPlot('myDiv', data, layout);
    </script>
    
    """
    return string

if __name__ == "__main__":
    path=sys.argv[1]    
    f = open(path+'.html','w')
    f.write(construction_data(path))
    f.close()
    

#
#path='/home/t/Bureau/concept_full/0/graph_3'
#f = open(path+'.tmp','r')
#data=f.read()
#f.close() 
#tx=data.split('=')[1].split('},')
#dataf={}
#text=""
#textdata=''
#list_color=['#17BECF','#C2185B','#1A237E','#651fff','#d50000','#00c853','#ffff00','#dd2c00','#212121','#000000']
#for k in range(len(tx)):
#    x=[]
#    y=[]
#    name=''
#    for el in tx[k].split("'x':")[1].split("'y':")[0].replace('],','').split(','):
#            x.append(el.split('name')[0].replace('[','').replace("'","").replace(" ",''))
#    for el in tx[k].split("'y':")[1].split("'x':")[0].split('name')[0].replace('],','').split(','):
#            y.append(int(el.replace('[','').replace(' ','').replace("'",''))) 
#    name=tx[k].split("'x':")[1].split("'y':")[0].split('name')[1].replace("':'",'').replace('}','').replace(":",'').replace("'",'').replace(" ",'')
#    dataf[k+1]={'x':x,'y':y,'name':name}