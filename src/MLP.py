#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 00:58:35 2020

@author: t
"""

import sys
import numpy as np
from sklearn.neural_network import MLPClassifier,MLPRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression
from util import MASE_cal
from util import get_data    
from util import writeelement
from util import save_model_high


if __name__ == "__main__":
    path=sys.argv[1]    
    data=get_data(path)
    model=MLPRegressor(activation=str(sys.argv[5]),solver=str(sys.argv[6]), alpha=float(sys.argv[7]),hidden_layer_sizes=(int(sys.argv[8]),int(sys.argv[9])))
    memoire=[100000];
    x=np.array(range(1,len(data[1]['x'])+1)).reshape(-1, 1)
    y_max=None
    for i in range(0,100):
        Y_train=data[1]['y']
        model.fit(x,Y_train)
        #to predict
        y_train_predict =model.predict(x)
        rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
        r2 =model.score(x,y_train_predict)
        mase_score=MASE_cal(data[1]['x'],Y_train,y_train_predict)
        if min(memoire)>mase_score:
            memoire.append(mase_score)
            y_max=y_train_predict
            save_model_high(path,model,sys.argv[3],r2,rmse,mase_score)
    writeelement(sys.argv[2],data[1]['x'],y_max,sys.argv[3],sys.argv[4])
    memoire=None        


           
#
#
##Evaluation du training set
#y_train_predict = lmodellineaire.predict(X_train)
#rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
#r2 = r2_score(Y_train, y_train_predict)
# 
#print('La performance du modèle sur la base dapprentissage')
#print('--------------------------------------')
#print('Lerreur quadratique moyenne est {}'.format(rmse))
#print('le score R2 est {}'.format(r2))
#print('\n')
# 
## model evaluation for testing set
#y_test_predict = lmodellineaire.predict(X_test)
#rmse = (np.sqrt(mean_squared_error(Y_test, y_test_predict)))
#r2 = r2_score(Y_test, y_test_predict)
# 
#print('La performance du modèle sur la base de test')
#print('--------------------------------------')
#print('Lerreur quadratique moyenne est {}'.format(rmse))
#print('le score R2 est {}'.format(r2))
