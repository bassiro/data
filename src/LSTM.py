#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 18:10:20 2020

@author: t
"""
import numpy as np
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error

from util import MASE_cal
from util import preprocessing

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras import backend as K
    
import tensorflow as tf
import tensorflow
tf.reset_default_graph()


import os
import sys

def create_file(site, product,Expected, Predicted):
    Predicted=np.array(Predicted).astype(int)  
    try:
        f = open(str(site)+'_'+str(product)+".csv","w")
        f.writelines(str(Expected))
        f.writelines('\n')
        f.writelines(str(Predicted))
        f.writelines('\n')
        f.close()
    except:
        pass
    return Expected,Predicted



def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = pd.DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
# put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


def retrouve_file(Data):
    T=[]
    for i in range(len(Data)):
        if Data.shape[1]==4:
            T.append(Data[i][3])
        else:
            T.append(Data[i][2])
    return T

def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return pd.Series(diff)

def prepare_data1(series, n_test, n_lag, n_seq):
    # extract raw values
    
    raw_values = series.values
    diff_series = difference(raw_values, 1)
    #print(diff_series)
    diff_values = diff_series.values
    #print(diff_values)
    diff_values = diff_values.reshape(len(diff_values), 1)
    # rescale values to -1, 1
    
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaled_values = scaler.fit_transform(diff_values)
    scaled_values = scaled_values.reshape(len(scaled_values), 1)
    # transform into supervised learning problem X, y
    scaled_values= scaled_values.tolist()
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values
    # split into train and test sets
    train, test = supervised_values[0:-n_test], supervised_values[-n_test:]
    return scaler, train, test

def fit_lstm(train, n_lag, n_seq, n_batch, nb_epoch, n_neurons):
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    X = X.reshape(X.shape[0], 1, X.shape[1])
    # design network
    model = Sequential()
    model.add(LSTM(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=True))
    model.add(Dense(y.shape[1]))
    model.compile(loss='mean_squared_error', optimizer='adam')
    # fit network
    for i in range(nb_epoch):
        model.fit(X, y, epochs=1, batch_size=n_batch, verbose=0, shuffle=False)
        model.reset_states()
    return model


def forecast_lstm(model, X, n_batch):
   
    X = X.reshape(1, 1, len(X))
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


def make_forecasts(model, n_batch, train, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch)
        # store the forecast
        forecasts.append(forecast)
    return forecasts

def inverse_difference(last_ob, forecast):
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i-1])
    return inverted


def inverse_transform(series, forecasts, scaler, n_test):
    inverted = list()
    for i in range(len(forecasts)):
    # create array from forecast
        forecast = np.array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]
        # invert differencing
        index = len(series) - n_test + i - 1
        if -index >= len(series) or index >= len(series):
             inverted.append(inv_scale)
        else:
             last_ob = series.values[index]
        #print(last_ob)
             inv_diff = inverse_difference(last_ob, inv_scale)
    
        #print(inv_diff)
             inverted.append(inv_diff)
    return inverted

def run_model(data,site,product,K,n_test):
    n_epochs = 100
    n_batch = 1
    n_neurons = 4
    n_lag =1
    n_seq=3
    Data1=data.query('site_code=='+str(site) + ' and product_code=='+str(product)) 
    if len(Data1) > 1:
       dataset=Data1.loc[:,'distributed']
       scaler, train, test = prepare_data1(dataset, n_test, n_lag, n_seq)
       if test.size ==0:
          create_file(site,product,[],[])
       else:
           # transform the scale of the data
           K.clear_session()
           model = fit_lstm(train, n_lag, n_seq, n_batch, n_epochs, n_neurons)
           # forecast the entire training dataset to build up state for forecasting
           forecasts = make_forecasts(model, n_batch, train, test, n_lag, n_seq)
           forecasts = inverse_transform(dataset, forecasts, scaler, n_test+2)
           predicted=retrouve_file(np.asarray(forecasts))
           actual = [row[n_lag:] for row in test]
           actual = np.around(inverse_transform(dataset, actual, scaler, n_test+2))
           #RMSE=evaluate_forecasts(actual, forecasts, n_lag, n_seq)
           expected=retrouve_file(actual)
           create_file(site, product,expected,predicted)
           

########################################Function  ##############################################
    
if __name__ == "__main__":
    
    base_directory=sys.argv[1]
    logistics_df = pd.read_csv(os.path.join(base_directory, 'contraceptive_logistics_data.csv'), parse_dates={'months': ['year','month']}) #main data_base
    product_df = pd.read_csv(os.path.join(base_directory, 'product.csv'))
    Logi=logistics_df.sort_values(by=['months'], ascending=True)
    #Logistics=logistics_df.set_index(['months'])
    logis=preprocessing(Logi)
    Logistic=logis.set_index(['months'])
    #Logistic
    Site=Logistic['site_code']
    Product=np.array(Logistic['product_code'])
    Distrib=np.array(Logistic['stock_distributed'])

    #Average=np.array(Logistics['average_monthly_consumption'])
    #Initial=np.array(Logistics['stock_initial'])
    #End= np.array(Logistics['stock_end'])
    #mth=np.array(logistics_df['months'])
    Dico = {'site_code': Site, 'product_code': Product, 'distributed': Distrib}#, 'Average_monthly': Average, 'Stock_init': Initial, 'Stock_end': End}  
    data= pd.DataFrame(Dico)
    Sit=np.unique(Logistic['site_code'])
    Prod=np.unique(Logistic['product_code'])
    Data1=data.query('site_code==0 and product_code==9')
    DataT=Data1.loc[:,'distributed']#.tolist()
    dataset = Data1['distributed']

    n_test= int(len(DataT) * 0.33)
    #K.clear_session()
    K=1
    #run_model(data,site,product,K)
    run_model(data,sys.argv[2],sys.argv[3],K,n_test)

