#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 21:22:56 2020
@author: traore
"""
import numpy as np
from random import sample 
from sklearn.neural_network import MLPClassifier,MLPRegressor
from sklearn.metrics import mean_squared_error # RMSE
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn import linear_model
from sklearn import svm
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)
#from sklearn.cross_validation import KFold
from sklearn.neighbors import KNeighborsRegressor
#from sklearn.naive_bayes import CategoricalNB
from sklearn import tree
from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.externals import joblib
import os
import sys

import pandas as pd 
import numpy as np
import json

def MASE_cal(Xval,Yval,Yp):
    """exemple d'utilisation
       Yp=ypredite     
       Yval=real
        Xval=['2016-06','2016-10','2017-04','2017-01','2017-05']
        Yval=[10,20,25,30,40]
        Yp=[12,23,26,27,50]
        MASE_cal(Xval,Yval,Yp)
        >>> 3.612448                 
    """
    
    X=np.array([Xval,Yval])
    Xp=np.array([Xval,Yp])
    X.sort(axis=1)
    Xp.sort(axis=1)
    #print(Xp)
    n=X.shape[1]
    d=1./(n-1)
    su=0
    for k in range(1,n):
        su=su+(float(X[1][k])-float(X[1][k-1]))
    div=su*d
    vectuq=X
    val=0
    for j in range(0,n):
        vectuq[1][j]=(np.abs(float(X[1][j])-float(Xp[1][j]))/div) 
        val=val+(np.abs(float(X[1][j])-float(Xp[1][j]))/div)
    return val/n


def savemodel(func_to_decorate):
    def new_func(*original_args, **original_kwargs):
        print("Function has been decorated.  Congratulations."+str(func_to_decorate))
        # Do whatever else you want here
        #memoire_score[str(func_to_decorate)]=func_to_decorate(*original_args, **original_kwargs)
        return func_to_decorate(*original_args, **original_kwargs)
    return new_func



def sauvergarde_file_optimal(chemin,clf_tree,score_tree,name_model,memoire_score):
        " Call une varaible global pour voir ce qui était dans sa memoire comme modèle optimal"
        try:
            
           if score_tree<=memoire_score[name_model] and str(score_tree)!='nan':
              joblib.dump(clf_tree,chemin+name_model+'.pkl')
              memoire_score[name_model]=score_tree
           else:
               print("Not Max:")
        except:
            memoire_score[name_model]=score_tree
            if str(score_tree)!='nan':
                joblib.dump(clf_tree, chemin+name_model+'.pkl')
            pass
        try:
            f = open(chemin+"dict.json","w")
            f.write(str(memoire_score))
            f.close()
        except:
            pass 
        try:
            f = open(chemin+"goodmodel.json","w")
            min=1000000
            name=''
            for el in memoire_score.keys():
                if min>=memoire_score[el]:
                    name=el
                    min=memoire_score[el]
            f.write(str(name))
            f.close()
        except:
            pass        
        #print(os.getcwd()+'/model/'+name_model+'.pkl')




#Supervised learning


def linear_model_OLS(X_train, y_train,X_test, y_test,reg,Xp,Yval,X,chemin,memoire_score):
    "Ordinary Least Squares"
    try:
        reg.fit(X_train, y_train)
        print("Linear model Ordinary Least Squares")
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=reg.predict(X_val_test)
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)        
        
        print("MASE:"+str(score_val))            
 
        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'linear_model_OLS',memoire_score)
        #sauvergarde_file_optimal(chemin,reg,score,'linear_model_OLS')
    except:
        pass



def linear_model_Ridge(X_train, y_train,X_test, y_test,alph,reg,Xp,Yval,X,chemin,memoire_score):
    "Ridge"
    try:
        reg.fit(X_train, y_train)
        print("Linear model Ridge"+str(alph))
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)
        
        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=reg.predict(X_val_test)
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)      
        print("MASE:"+str(score_val))            
        
        
        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'linear_model_Ridge',memoire_score)
    except:
        pass

def BayesianRidge_m(X_train, y_train,X_test, y_test,reg,Xp,Yval,X,chemin,memoire_score):
    "Ridge"
    try:
        #reg=linear_model.BayesianRidge(compute_score=statut)
        reg.fit(X_train, y_train)
        print("BayesianRidge_m ")
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)
        
        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=reg.predict(X_val_test)
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)     
        print("MASE:"+str(score_val))          
        
        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'BayesianRidge_m',memoire_score)
    except:
        pass

def SGDRegressor_m(X_train, y_train,X_test, y_test,reg,Xp,Yval,X,chemin,memoire_score):
    "SGDRegressor"
    try:
        #reg=linear_model.SGDRegressor(loss=statut,max_iter=1000, tol=0.001)
        reg.fit(X_train, y_train)
        print("SGDRegressor_m")
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)
        
        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)
        print("MASE:"+str(score_val))        
        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'SGDRegressor_m',memoire_score)
    except:
        pass


def svm_m(X_train, y_train,X_test, y_test,clf_SVR,Xp,Yval,X,chemin,memoire_score):
        "svm"
        try:
            #if ty=="svm":
            #    clf_SVR = svm.SVR()
            #elif ty=="LinearSVR":
            #    clf_SVR = svm.LinearSVR()
            #else:
            #    pass
            clf_SVR.fit(X_train, y_train)
            score_SVR = clf_SVR.score(X_test, y_test)
            print(" SVM ")
            print(score_SVR)
            y_pred=clf_SVR.predict(X_test)
            
            y_cal=[]
            index=range(len(X))
            X_val_test=[]
            for k in index:
                X_val_test.append(X[k])
            X_val_test=np.array(X_val_test)
            y_c=clf_SVR.predict(X_val_test)
            y_cal1=[]
            for j in y_c:
                y_cal1.append(np.round(j,0))
            
            score_val=MASE_cal(Xp,Yval,y_cal1)
            print("MASE:"+str(score_val))
            print("RMSE:"+str(mean_squared_error(y_test, y_pred)))
            
            "sauvegarder le modele"
            sauvergarde_file_optimal(chemin,clf_SVR,score_val,'svm_m',memoire_score)
        except:
            pass     


def gaussian_process_m(X_train, y_train,X_test, y_test,gpr,Xp,Yval,X,chemin,memoire_score):
        "gaussian_process"
        try:
            gpr.fit(X_train, y_train)
            score_gpr = gpr.score(X_test, y_test)

            y_cal=[]
            index=range(len(X))
            X_val_test=[]
            for k in index:
                X_val_test.append(X[k])
            X_val_test=np.array(X_val_test)
            y_c=gpr.predict(X_val_test)
                
            y_cal1=[]
            for j in y_c:
                y_cal1.append(np.round(j,0))
            
            score_val=MASE_cal(Xp,Yval,y_cal1)            
            print("MASE:"+str(score_val))            
 
            print(score_gpr)
            y_pred=gpr.predict(X_test)
            print("RMSE:"+str(mean_squared_error(y_test, y_pred)))
            "sauvegarder le modele"
            sauvergarde_file_optimal(chemin,gpr,score_val,'gaussian_process_m',memoire_score)
        except:
            pass        


#def naiveBaye_m(X_train, y_train,X_test, y_test):
#    "naaive bayes"
  
def treeDecision_regresion_m(X_train, y_train,X_test, y_test,clf_tree,Xp,Yval,X,chemin,memoire_score):
    #tree Descion
    score_tree=0
    try:
        #"regtreeDecision_regresion_m"+str(maxdep)+"=tree.DecisionTreeRegressor(max_depth="+str(maxdep)+")"
        clf_tree.fit(X_train, y_train)
        score_tree = clf_tree.score(X_test, y_test)
        print(" arbre de decision")

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=clf_tree.predict(X_val_test)

        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)
        
        print("MASE:"+str(score_val))            
 

        print(score_tree)
        y_pred=clf_tree.predict(X_test)
        print("RMSE:"+str(mean_squared_error(y_test, y_pred)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,clf_tree,score_val,'treeDecision_regresion_m',memoire_score)
        #sauvergarde_file_optimal(chemin,clf_tree,score_tree,'treeDecision_regresion_m')
    except:
        pass
    return  score_val    

 
# randomForestRegression         
def RandomForestRegressor_m(X_train, y_train,X_test, y_test,clf_tree,Xp,Yval,X,chemin,memoire_score):
    #tree Descion
    score_tree=0
    try:
        #clf_tree = RandomForestRegressor(max_depth=maxt, random_state=0)
        clf_tree.fit(X_train, y_train)
        score_tree = clf_tree.score(X_test, y_test)
        print(" random forest ")

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=clf_tree.predict(X_val_test)
            
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)        
        print("MASE:"+str(score_val))            
 

        print(score_tree)
        y_pred=clf_tree.predict(X_test)
        print("RMSE:"+str(mean_squared_error(y_test, y_pred)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,clf_tree,score_val,'RandomForestRegressor_m',memoire_score)
    except:
        pass
    return score_val

      
def MLPregression(X_train, y_train,X_test, y_test,modelMLPR,Xp,Yval,X,chemin,memoire_score):
    try:
        # version classification
        #if solver in ["lbfgs","sgd","adam"]:
        #    modelMLPR = MLPRegressor(activation=activation1,solver=solver, alpha=1e-7,hidden_layer_sizes=size_hidden,)
        #else:
        #    solver='lbfgs'
        #    modelMLPR = MLPRegressor(activation=activation1,solver=solver, alpha=1e-7,hidden_layer_sizes=size_hidden)
        
        #to learn
        modelMLPR.fit(X_train, y_train)
        #to predict
        print("Neural network ")
        yTest_predicted =modelMLPR.predict(X_test)
        score=modelMLPR.score(X_test, y_test)

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=modelMLPR.predict(X_val_test)
            
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)
        
        print("MASE:"+str(score_val))            
 


        print("Score:"+str(score))
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        sauvergarde_file_optimal(chemin,modelMLPR,score_val,'MLPregression',memoire_score)
    except:
        pass


def KNeighborsRegressor_m(X_train, y_train,X_test, y_test,reg,Xp,Yval,X,chemin,memoire_score):
    try:
        reg.fit(X_train, y_train)
        print("KNeighborsRegressor_m")
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=reg.predict(X_val_test)
            
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)
        
        print("MASE:"+str(score_val))            
 

        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'KNeighborsRegressor_m',memoire_score)
    except:
        pass
    
def AdaBoostRegressor_m(X_train, y_train,X_test, y_test,reg,Xp,Yval,X,chemin,memoire_score):
    try:
        #reg 
        reg.fit(X_train, y_train)
        print("AdaBoostRegressor_m")
        yTest_predicted =reg.predict(X_test)
        score=reg.score(X_test, y_test)

        y_cal=[]
        index=range(len(X))
        X_val_test=[]
        for k in index:
            X_val_test.append(X[k])
        X_val_test=np.array(X_val_test)
        y_c=reg.predict(X_val_test)
            
            
        y_cal1=[]
        for j in y_c:
            y_cal1.append(np.round(j,0))
        
        score_val=MASE_cal(Xp,Yval,y_cal1)
        print("MASE:"+str(score_val))            
 


        print(score)
        print("RMSE:"+str(mean_squared_error(y_test, yTest_predicted)))
        "sauvegarder le modele"
        sauvergarde_file_optimal(chemin,reg,score_val,'AdaBoostRegressor_m',memoire_score)
    except:
        pass    
    
    
def calcul_gini(actual_list, predictions_list):
    try:
        fpr, tpr, thresholds = roc_curve(actual_list, predictions_list)
        roc_auc = auc(fpr, tpr)
        GINI = (2 * roc_auc) - 1
        return GINI
    except:
        return "False"
        pass 
    

def run_model(chemin,X,Y,perc,nbre_fois,model_name,list_variable,g,memoire_score):
    regRandomForestRegressor_m1,regRandomForestRegressor_m2,regRandomForestRegressor_m3,regRandomForestRegressor_m4,regRandomForestRegressor_m5=None,None,None,None,None
    "ensemble des modeles crées"
    if 'RandomForestRegressor_m' in model_name:
        regRandomForestRegressor_m1=RandomForestRegressor(max_depth=2, random_state=0)
        regRandomForestRegressor_m2=RandomForestRegressor(max_depth=7, random_state=0)
        regRandomForestRegressor_m3=RandomForestRegressor(max_depth=12, random_state=0)
        regRandomForestRegressor_m4=RandomForestRegressor(max_depth=14, random_state=0)
        regRandomForestRegressor_m5=RandomForestRegressor(max_depth=18, random_state=0)

    if 'treeDecision_regresion_m' in model_name:
        regtreeDecision_regresion_m1=tree.DecisionTreeRegressor(max_depth=2)
        regtreeDecision_regresion_m2=tree.DecisionTreeRegressor(max_depth=7)
        regtreeDecision_regresion_m3=tree.DecisionTreeRegressor(max_depth=12)
        regtreeDecision_regresion_m4=tree.DecisionTreeRegressor(max_depth=14)
        regtreeDecision_regresion_m5=tree.DecisionTreeRegressor(max_depth=18)

    if 'svm_m' in model_name:
        regvm_m1=svm.SVR()
        regvm_m2=svm.LinearSVR()                
    
    if 'linear_model_OLS' in model_name:
        reglinearie = linear_model.LinearRegression()
    if 'KNeighborsRegressor_m' in model_name:
        regKNeighborsRegressor=KNeighborsRegressor(n_neighbors=2)
    
    if 'linear_model_Ridge' in model_name:
        reglinear_model_Ridge1 = linear_model.Ridge(alpha=0.1)
        reglinear_model_Ridge2 = linear_model.Ridge(alpha=0.1)
        reglinear_model_Ridge3 = linear_model.Ridge(alpha=0.1)
        reglinear_model_Ridge4 = linear_model.Ridge(alpha=0.1)
        reglinear_model_Ridge5 = linear_model.Ridge(alpha=0.5)
        reglinear_model_Ridge6 = linear_model.Ridge(alpha=0.6)
        reglinear_model_Ridge7 = linear_model.Ridge(alpha=0.7)
        reglinear_model_Ridge8 = linear_model.Ridge(alpha=0.8)
        reglinear_model_Ridge9 = linear_model.Ridge(alpha=0.9)
    if 'BayesianRidge_m' in model_name:
        regBayesianRidge_m_F=linear_model.BayesianRidge(compute_score=False)
        regBayesianRidge_m_T=linear_model.BayesianRidge(compute_score=True)
    if 'AdaBoostRegressor_m' in model_name:
        regAdaBoostRegressor_m=AdaBoostRegressor(random_state=0, n_estimators=100)
    if 'MLPregression' in model_name:
        reqMLPregression1=MLPRegressor(activation="identity",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression2=MLPRegressor(activation="identity",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression3=MLPRegressor(activation="identity",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression4=MLPRegressor(activation="logistic",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression5=MLPRegressor(activation="logistic",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression6=MLPRegressor(activation="logistic",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression7=MLPRegressor(activation="tanh",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression8=MLPRegressor(activation="tanh",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression9=MLPRegressor(activation="tanh",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression10=MLPRegressor(activation="relu",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression11=MLPRegressor(activation="relu",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression12=MLPRegressor(activation="relu",solver="lbfgs", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression13=MLPRegressor(activation="identity",solver="sgd", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression14=MLPRegressor(activation="identity",solver="sgd", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression15=MLPRegressor(activation="identity",solver="sgd", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression16=MLPRegressor(activation="logistic",solver="sgd", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression17=MLPRegressor(activation="logistic",solver="sgd", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression18=MLPRegressor(activation="logistic",solver="sgd", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression19=MLPRegressor(activation="tanh",solver="sgd", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression20=MLPRegressor(activation="tanh",solver="sgd", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression21=MLPRegressor(activation="relu",solver="sgd", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression22=MLPRegressor(activation="relu",solver="sgd", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression23=MLPRegressor(activation="relu",solver="sgd", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression24=MLPRegressor(activation="relu",solver="sgd", alpha=1e-7,hidden_layer_sizes=(20,1))     
        reqMLPregression25=MLPRegressor(activation="identity",solver="adam", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression26=MLPRegressor(activation="identity",solver="adam", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression27=MLPRegressor(activation="identity",solver="adam", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression28=MLPRegressor(activation="logistic",solver="adam", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression29=MLPRegressor(activation="logistic",solver="adam", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression30=MLPRegressor(activation="logistic",solver="adam", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression31=MLPRegressor(activation="tanh",solver="adam", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression32=MLPRegressor(activation="tanh",solver="adam", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression33=MLPRegressor(activation="tanh",solver="adam", alpha=1e-7,hidden_layer_sizes=(20,1))
        reqMLPregression34=MLPRegressor(activation="relu",solver="adam", alpha=1e-7,hidden_layer_sizes=(100,10))
        reqMLPregression35=MLPRegressor(activation="relu",solver="adam", alpha=1e-7,hidden_layer_sizes=(70,30))
        reqMLPregression36=MLPRegressor(activation="relu",solver="adam", alpha=1e-7,hidden_layer_sizes=(20,1))        
    if 'GaussianProcessRegressor_m' in model_name:
        kernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                       1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                       1.0 * ExpSineSquared(length_scale=1.0, periodicity=3.0,
                                            length_scale_bounds=(0.1, 10.0),
                                            periodicity_bounds=(1.0, 10.0)),
                       ConstantKernel(0.1, (0.01, 10.0))
                           * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
                       1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0),
                                    nu=1.5),DotProduct() + WhiteKernel()]
        gpr1 = GaussianProcessRegressor(kernel=kernels[0],random_state=0)
        gpr2 = GaussianProcessRegressor(kernel=kernels[1],random_state=0)
        gpr3 = GaussianProcessRegressor(kernel=kernels[2],random_state=0)
        gpr4 = GaussianProcessRegressor(kernel=kernels[3],random_state=0)
        gpr5 = GaussianProcessRegressor(kernel=kernels[4],random_state=0)
        gpr6 = GaussianProcessRegressor(kernel=kernels[5],random_state=0)

    if 'SGDRegressor_m' in model_name:
        regSGDRegressor_m1=linear_model.SGDRegressor(loss="squared_loss",max_iter=1000, tol=0.001)  
        regSGDRegressor_m2=linear_model.SGDRegressor(loss="huber",max_iter=1000, tol=0.001)       
        regSGDRegressor_m3=linear_model.SGDRegressor(loss="epsilon_insensitive",max_iter=1000, tol=0.001)       
        regSGDRegressor_m4=linear_model.SGDRegressor(loss="squared_epsilon_insensitive",max_iter=1000, tol=0.001)                  
    #X=X[list(list_variable)]
    #print(np.array(X))
    
    X=np.array(X)
    Y=np.array(Y)
    X_f=[]
    Xp=[]
    Yval=[]
    for k in range(0,len(X)):
        date_t=''
        if X[k][1]>9:
           date_t=str(X[k][0])+'-'+str(X[k][1])+'-01'
        else:
           date_t=str(X[k][0])+'-0'+str(X[k][1])+'-01'
        Xp.append(date_t)
        Yval.append(Y[k])
        X_f.append(X[k])
    for vhj in range(0,g):
        for k in range(1,nbre_fois):
            train_index=[]
            index=range(len(X))
            nb=int(perc)
            test_index=sample(list(index),nb)
            for k in list(index):
                  if k not in test_index:
                     train_index.append(int(k))
            
            X_test=X[test_index] # Chargement du X test
            y_test=Y[test_index] # Chargement du y test
            X_train=X[train_index] # Chargement du X train
            y_train=Y[train_index] # Chargement du y train   
            """    
            train_index=[]
            test_index=sample(list(X.index),perc)
            for k in list(X.index):
                if k not in test_index:
                    train_index.append(k)
            X_test=X.iloc[test_index] # Chargement du X test
            y_test=Y.iloc[test_index] # Chargement du y test
        
            X_train=X.iloc[train_index] # Chargement du X train
            y_train=Y.iloc[train_index] # Chargement du y train 
            """
            
            if 'RandomForestRegressor_m' in model_name:
                # arbre de decision
                RandomForestRegressor_m(X_train, y_train,X_test, y_test,regRandomForestRegressor_m1,Xp,Yval,X_f,chemin,memoire_score)
                RandomForestRegressor_m(X_train, y_train,X_test, y_test,regRandomForestRegressor_m2,Xp,Yval,X_f,chemin,memoire_score)
                RandomForestRegressor_m(X_train, y_train,X_test, y_test,regRandomForestRegressor_m3,Xp,Yval,X_f,chemin,memoire_score)
                RandomForestRegressor_m(X_train, y_train,X_test, y_test,regRandomForestRegressor_m4,Xp,Yval,X_f,chemin,memoire_score)
                RandomForestRegressor_m(X_train, y_train,X_test, y_test,regRandomForestRegressor_m5,Xp,Yval,X_f,chemin,memoire_score)
    
            if 'treeDecision_regresion_m' in model_name:
                treeDecision_regresion_m(X_train, y_train,X_test, y_test,regtreeDecision_regresion_m1,Xp,Yval,X_f,chemin,memoire_score)
                treeDecision_regresion_m(X_train, y_train,X_test, y_test,regtreeDecision_regresion_m2,Xp,Yval,X_f,chemin,memoire_score)
                treeDecision_regresion_m(X_train, y_train,X_test, y_test,regtreeDecision_regresion_m3,Xp,Yval,X_f,chemin,memoire_score)
                treeDecision_regresion_m(X_train, y_train,X_test, y_test,regtreeDecision_regresion_m4,Xp,Yval,X_f,chemin,memoire_score)
                treeDecision_regresion_m(X_train, y_train,X_test, y_test,regtreeDecision_regresion_m5,Xp,Yval,X_f,chemin,memoire_score)
            if 'MLPregression' in model_name:
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression1,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    print("Toto")
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression2,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression3,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression4,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression5,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression6,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression7,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression8,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression9,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression10,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression11,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression12,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression13,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression14,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression15,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression16,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass 
                try:
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression17,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression18,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression19,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression20,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression21,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression22,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression23,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression24,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression25,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression26,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression27,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression28,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression29,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression30,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression31,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression32,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression33,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression34,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression35,Xp,Yval,X_f,chemin,memoire_score)
                    MLPregression(X_train, y_train,X_test, y_test,reqMLPregression36,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass             
            if 'AdaBoostRegressor_m' in model_name:
                    try:
                        AdaBoostRegressor_m(X_train, y_train,X_test, y_test,regAdaBoostRegressor_m,Xp,Yval,X_f,chemin,memoire_score)
                    except:
                        pass
            if 'KNeighborsRegressor_m' in model_name:
                try:
                    KNeighborsRegressor_m(X_train, y_train,X_test, y_test,regKNeighborsRegressor,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                
        
            "model lineaire"
            if 'linear_model_OLS' in model_name:
                try:
                    "Ordinary Least Squares"
                    linear_model_OLS(X_train, y_train,X_test, y_test,reglinearie,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
            if 'linear_model_Ridge' in model_name:
                #for alph in [0.1,0.4,0.5,0.6,0.7,0.8]:
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge1,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge2,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge3,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge4,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge5,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge6,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge7,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge8,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    "Ordinary Least Rigde"
                    linear_model_Ridge(X_train, y_train,X_test, y_test,reglinear_model_Ridge9,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
            if 'BayesianRidge_m' in model_name: 
                try:
                    BayesianRidge_m(X_train, y_train,X_test, y_test,regBayesianRidge_m_T,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    BayesianRidge_m(X_train, y_train,X_test, y_test,regBayesianRidge_m_F,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
            if 'SGDRegressor_m' in model_name:        
                #for statut in ["squared_loss","huber","epsilon_insensitive","squared_epsilon_insensitive"]:
                try:
                    SGDRegressor_m(X_train, y_train,X_test, y_test,regSGDRegressor_m1,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    SGDRegressor_m(X_train, y_train,X_test, y_test,regSGDRegressor_m2,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    SGDRegressor_m(X_train, y_train,X_test, y_test,regSGDRegressor_m3,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    SGDRegressor_m(X_train, y_train,X_test, y_test,regSGDRegressor_m4,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                print("tot")
            
            if 'svm_m' in model_name:
                #"discrimation"
                try:
                    svm_m(X_train, y_train,X_test, y_test,regvm_m1,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
                try:
                    svm_m(X_train, y_train,X_test, y_test,regvm_m2,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
    
            if 'gaussian_process_m' in model_name:                
                #gaussian_process_m
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr1,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass    
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr2,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass       
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr3,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass  
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr4,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass    
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr5,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass       
                try:
                    gaussian_process_m(X_train, y_train,X_test, y_test,gpr6,Xp,Yval,X_f,chemin,memoire_score)
                except:
                    pass
        