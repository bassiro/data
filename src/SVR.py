#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 00:58:35 2020

@author: t
"""

import sys
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn import svm
from sklearn.metrics import mean_squared_error # RMSE
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_curve, auc
from util import MASE_cal
from util import get_data
from util import writeelement
from util import save_model_high


if __name__ == "__main__":
    path=sys.argv[1]    
    data=get_data(path)
    clf_SVR=svm.SVR()
    memoire=[100000];
    x=np.array(range(1,len(data[1]['x'])+1)).reshape(-1, 1)
    y_max=None
    for i in range(0,10):
        clf_SVR.fit(x, data[1]['y'])
        Y_train=data[1]['y']
        y_train_predict =clf_SVR.predict(x)
        rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
        #r2 = r2_score(Y_train, y_train_predict)
        r2 = clf_SVR.score(x,y_train_predict)
        mase_score=MASE_cal(data[1]['x'],Y_train,y_train_predict)
        if min(memoire)>mase_score:
            memoire.append(mase_score)
            y_max=y_train_predict
            save_model_high(path,clf_SVR,sys.argv[3],r2,rmse,mase_score)
    writeelement(sys.argv[2],data[1]['x'],y_max,sys.argv[3],sys.argv[4])
    memoire=None        
