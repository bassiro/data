#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 00:58:35 2020

@author: t
"""

import sys
import numpy as np
from sklearn.externals import joblib
import os
from os import listdir
from os.path import isfile, join


## for model LSTM 
def encodage(data_frame):
    for col in data_frame.select_dtypes('object'):
        data_frame[col]=data_frame[col].map(code)
    return data_frame

def epuration(data_frame):
    return data_frame.dropna(axis=0)

def preprocessing(data_frame):
    data_frame = encodage(data_frame)
    data_frame=epuration(data_frame)
    return data_frame




def MASE_cal(Xval,Yval,Yp):
    """exemple d'utilisation
       Yp=ypredite     
       Yval=real
        Xval=['2016-06','2016-10','2017-04','2017-01','2017-05']
        Yval=[10,20,25,30,40]
        Yp=[12,23,26,27,50]
        MASE_cal(Xval,Yval,Yp)
        >>> 3.612448                 
    """
    valucal=None
    #output_plot=[]
    X=np.array([Xval,Yval])
    Xp=np.array([Xval,Yp])
        
    X.sort(axis=1)
    Xp.sort(axis=1)
    n=X.shape[1]
    d=1./(n-1)
    su=0.00000000001
        
    for k in range(1,n):
        su=su+(float(X[1][k])-float(X[1][k-1]))
        
    div=su*d
    vectuq=X
    val=0
    for j in range(0,n):
        vectuq[1][j]=(np.abs(float(X[1][j])-float(Xp[1][j]))/div) 
        val=val+(np.abs(float(X[1][j])-float(Xp[1][j]))/div)
    valucal=val/n
    return valucal


def get_data(path):
    f = open(path+'.tmp','r')
    data=f.read()
    f.close() 
    tx=data.split('=')[1].split('},')
    dataf={}
    text=""
    textdata=''
    for k in range(len(tx)):
        x=[]
        y=[]
        name=''
        for el in tx[k].split("'x':")[1].split("'y':")[0].replace('],','').split(','):
                x.append(el.split('name')[0].replace('[','').replace("'","").replace(" ",''))
        for el in tx[k].split("'y':")[1].split("'x':")[0].split('name')[0].replace('],','').split(','):
                y.append(int(el.replace('[','').replace(' ','').replace("'",''))) 
        name=tx[k].split("'x':")[1].split("'y':")[0].split('name')[1].replace("':'",'').replace('}','').replace(":",'').replace("'",'').replace(" ",'')
        dataf[k+1]={'x':x,'y':y,'name':name}
    return dataf     


def writeelement(path,x,y,name,k):
    #dataplot=None
    #dataplot="""data={1:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'test'},2:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLP'},3:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLPGO'}}"""
    try:
        f = open(path+'.tmp','a')
        f.write(","+str(k)+":{'y':"+str(list(y))+",'x':"+str(x).replace("u'","'")+",'name': '"+str(name)+"'}")
        f.close()
    except:
        pass
    
    
def save_model_high(path,model,name,r2,rmse,mase_score):
    racine=path.split('graph_')[0]
    try:
        os.mkdir(racine+'model/')
    except:
        pass
    try:    
        os.mkdir(racine+'model/'+path.split('graph_')[1]+'/')
    except:
        pass
    newpath=racine+'model/'+path.split('graph_')[1]+'/'
    joblib.dump(model,newpath+name+'.pkl')
    f = open(newpath+name+'.score','w')
    f.write("r2:"+str(r2)+"\n")
    f.write("RMSE:"+str(rmse)+"\n")
    f.write("MASE:"+str(mase_score)+"\n")
    f.close()


code={'ABIDJAN 1-GRANDS PONTS': 0,
 'ABIDJAN 2': 1,
 'AGNEBY-TIASSA-ME': 2,
 'BELIER': 3,
 'BOUNKANI-GONTOUGO': 4,
 'CAVALLY-GUEMON': 5,
 'GBEKE': 6,
 'GBOKLE-NAWA-SAN PEDRO': 7,
 'GOH': 8,
 'HAMBOL': 9,
 'HAUT-SASSANDRA': 10,
 'INDENIE-DJUABLIN': 11,
 'KABADOUGOU-BAFING-FOLON': 12,
 'LOH-DJIBOUA': 13,
 'MARAHOUE': 14,
 "N'ZI-IFOU-MORONOU": 15,
 'PORO-TCHOLOGO-BAGOUE': 16,
 'SUD-COMOE': 17,
 'TONKPI': 18,
 'WORODOUGOU-BERE': 19,
 'ABENGOUROU': 0,
 'ABOBO-EST': 1,
 'ABOISSO': 2,
 'ADIAKE': 3,
 'ADJAME-PLATEAU-ATTECOUBE': 4,
 'ADZOPE': 5,
 'AGBOVILLE': 6,
 'AGNIBILEKROU': 7,
 'AKOUPE': 8,
 'ALEPE': 9,
 'ANYAMA': 10,
 'BANGOLO': 11,
 'BEOUMI': 12,
 'BETTIE': 13,
 'BIANKOUMA': 14,
 'BLOLEQUIN': 15,
 'BOCANDA': 16,
 'BONDOUKOU': 17,
 'BONGOUANOU': 18,
 'BOUAFLE': 19,
 'BOUAKE NORD-OUEST': 20,
 'BOUNA': 21,
 'BOUNDIALI': 22,
 'COCODY-BINGERVILLE': 23,
 'DABAKALA': 24,
 'DABOU': 25,
 'DALOA': 26,
 'DANANE': 27,
 'DAOUKRO': 28,
 'DIDIEVI': 29,
 'DIMBOKRO': 30,
 'DIVO': 31,
 'DUEKOUE': 32,
 'FERKESSEDOUGOU': 33,
 'FRESCO': 34,
 'GAGNOA': 35,
 'GRAND-BASSAM': 36,
 'GRAND-LAHOU': 37,
 'GUEYO': 38,
 'GUIGLO': 39,
 'GUITRY': 40,
 'ISSIA': 41,
 'JACQUEVILLE': 42,
 'KATIOLA': 43,
 'KORHOGO': 44,
 'KORO': 45,
 'KOUIBLY': 46,
 'KOUMASSI-PORT BOUET-VRIDI': 47,
 'LAKOTA': 48,
 "M'BAHIAKRO": 49,
 'MAN': 50,
 'MANKONO': 51,
 'MINIGNAN': 52,
 'NASSIAN': 53,
 'NIAKARAMADOUGOU': 54,
 'ODIENNE': 55,
 'OUANGOLO': 56,
 'OUME': 57,
 'PRIKRO': 58,
 'SAKASSOU': 59,
 'SAN PEDRO': 60,
 'SASSANDRA': 61,
 'SEGUELA': 62,
 'SIKENSI': 63,
 'SINFRA': 64,
 'SOUBRE': 65,
 'TABOU': 66,
 'TANDA': 67,
 'TENGRELA': 68,
 'TIASSALE': 69,
 'TIEBISSOU': 70,
 'TOUBA': 71,
 'TOULEPLEU': 72,
 'TOUMODI': 73,
 'TREICHVILLE-MARCORY': 74,
 'VAVOUA': 75,
 'YAMOUSSOUKRO': 76,
 'YOPOUGON-EST': 77,
 'YOPOUGON-OUEST-SONGON': 78,
 'ZOUAN-HOUNIEN': 79,
 'ZUENOULA': 80,
 'C1004': 0,
 'C1007': 1,
 'C1008': 2,
 'C1009': 3,
 'C1010': 4,
 'C1011': 5,
 'C1014': 6,
 'C1015': 7,
 'C1017': 8,
 'C1018': 9,
 'C1024': 10,
 'C1026': 11,
 'C1027': 12,
 'C1028': 13,
 'C1029': 14,
 'C1030': 15,
 'C1034': 16,
 'C1035': 17,
 'C1051': 18,
 'C1054': 19,
 'C1055': 20,
 'C1056': 21,
 'C1058': 22,
 'C1059': 23,
 'C1061': 24,
 'C1062': 25,
 'C1063': 26,
 'C1066': 27,
 'C1067': 28,
 'C1069': 29,
 'C1070': 30,
 'C1072': 31,
 'C1073': 32,
 'C1074': 33,
 'C1077': 34,
 'C1078': 35,
 'C1080': 36,
 'C1082': 37,
 'C1083': 38,
 'C1084': 39,
 'C1086': 40,
 'C1087': 41,
 'C1088': 42,
 'C1089': 43,
 'C1090': 44,
 'C1091': 45,
 'C1092': 46,
 'C1093': 47,
 'C1094': 48,
 'C1095': 49,
 'C1098': 50,
 'C1099': 51,
 'C1101': 52,
 'C1106': 53,
 'C1112': 54,
 'C1144': 55,
 'C1399': 56,
 'C1411': 57,
 'C1413': 58,
 'C1679': 59,
 'C1681': 60,
 'C1701': 61,
 'C1745': 62,
 'C2002': 63,
 'C2003': 64,
 'C2004': 65,
 'C2005': 66,
 'C2006': 67,
 'C2007': 68,
 'C2008': 69,
 'C2009': 70,
 'C2010': 71,
 'C2011': 72,
 'C2015': 73,
 'C2016': 74,
 'C2017': 75,
 'C2041': 76,
 'C2047': 77,
 'C2049': 78,
 'C2050': 79,
 'C2051': 80,
 'C2052': 81,
 'C2053': 82,
 'C2055': 83,
 'C2056': 84,
 'C2057': 85,
 'C2059': 86,
 'C2060': 87,
 'C2061': 88,
 'C2062': 89,
 'C2063': 90,
 'C2064': 91,
 'C2065': 92,
 'C2066': 93,
 'C2068': 94,
 'C2069': 95,
 'C2070': 96,
 'C2071': 97,
 'C2127': 98,
 'C2131': 99,
 'C2168': 100,
 'C2194': 101,
 'C2214': 102,
 'C3001': 103,
 'C3010': 104,
 'C3011': 105,
 'C3012': 106,
 'C3013': 107,
 'C3014': 108,
 'C3015': 109,
 'C3016': 110,
 'C3017': 111,
 'C3018': 112,
 'C3019': 113,
 'C3020': 114,
 'C3021': 115,
 'C3022': 116,
 'C3023': 117,
 'C3043': 118,
 'C4001': 119,
 'C4002': 120,
 'C4003': 121,
 'C4014': 122,
 'C4015': 123,
 'C4016': 124,
 'C4017': 125,
 'C4018': 126,
 'C4019': 127,
 'C4020': 128,
 'C4021': 129,
 'C4022': 130,
 'C4023': 131,
 'C4024': 132,
 'C4025': 133,
 'C4026': 134,
 'C4037': 135,
 'C4038': 136,
 'C4054': 137,
 'C4056': 138,
 'C4061': 139,
 'C5001': 140,
 'C5002': 141,
 'C5003': 142,
 'C5004': 143,
 'C5006': 144,
 'C5015': 145,
 'C5016': 146,
 'C5017': 147,
 'C5018': 148,
 'C5019': 149,
 'C5020': 150,
 'C5021': 151,
 'C5060': 152,
 'C5063': 153,
 'C5066': 154,
 'C5076': 155,
 'AS17005': 0,
 'AS21126': 1,
 'AS27000': 2,
 'AS27132': 3,
 'AS27133': 4,
 'AS27134': 5,
 'AS27137': 6,
 'AS27138': 7,
 'AS27139': 8,
 'AS42018': 9,
 'AS46000': 10}


def writeoutput(path,distr,prod,optimalmodel):
    #dataplot=None
    #dataplot="""data={1:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'test'},2:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLP'},3:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLPGO'}}"""
    try:
       os.mkdir(path+'/result/')
    except:
       pass
    try:
        f = open(path+'/result/prediction.csv','r')
        f.close()
    except:
        f = open(path+'/result/prediction.csv','w')
        f.write('"year","month","site_code","product_code","predicted_value"\n')
        f.close()
        pass
    try:
        f = open(path+'/result/prediction.csv','a')
        f.write('2019,10,'+str(distr)+',"'+str(prod)+'","'+optimalmodel+'"\n')
        #f.write(","+str(k)+":{'y':"+str(list(y))+",'x':"+str(x).replace("u'","'")+",'name': '"+str(name)+"'}")
        f.close()
    except:
        pass
    
def writescoreall(path,distr,prod,list_model,score='MASE'):
    #dataplot=None
    #dataplot="""data={1:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'test'},2:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLP'},3:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLPGO'}}"""
    optimalmodel='MPL_1'
    try:
       os.mkdir(path+'/result/')
    except:
       pass
    
    mypath=path+str(distr)+'/model/'+str(prod)
    try:
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f)) and 'score'in f ]    
    except:
        onlyfiles=[]
        pass
    output=[]
    for el in list_model:
        if el+'.score' not in onlyfiles:
            output.append(100000)
        else:
            val1=100000
            try:
                g = open(mypath+'/'+el+'.score','r')
                val=g.read()
                g.close()
                print(val)                
                val1=val.split('MASE')[1]
                val1=val1.split('\n')[0]
                val1=float(val1.replace(' ','').replace(':',''))
            except:
                pass
            output.append(round(float(val1),3))
   
    try:
        f = open(path+'/result/score.out','r')
        f.close()
    except:
        f = open(path+'/result/score.out','w')
        f.write('site_code,product_code,value,'+str(list_model)+'\n')
        f.close()
        pass        
    try:
      f = open(path+'/result/score.out','a')
      f.write(str(distr)+','+str(prod)+', value ,'+str(output).replace('[','').replace(']','').replace(':','')+'\n')
      #f.write(","+str(k)+":{'y':"+str(list(y))+",'x':"+str(x).replace("u'","'")+",'name': '"+str(name)+"'}")
      f.close()
    except:
      pass
    return list_model[np.argmin(output)]

#optimalmodel=writescoreall(path_out,distr,prod,list_model)
#writeoutput(path_out,distr,prod,optimalmodel)    