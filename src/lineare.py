#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 00:58:35 2020

@author: t
"""

import sys
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression
from util import MASE_cal
from util import get_data    
from util import writeelement
from util import save_model_high
if __name__ == "__main__":
    path=sys.argv[1]    
    data=get_data(path)
    lmodellineaire = LinearRegression()
    memoire=[100000];
    y_max=None
    for i in range(0,10):
        x=np.array(range(1,len(data[1]['x'])+1)).reshape(-1, 1)
        lmodellineaire.fit(x, data[1]['y'])
        Y_train=data[1]['y']
        y_train_predict = lmodellineaire.predict(x)
        rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
        r2 = r2_score(Y_train, y_train_predict)
        mase_score=MASE_cal(data[1]['x'],Y_train,y_train_predict)
        if min(memoire)>mase_score:
            memoire.append(mase_score)
            f = open(path+'.text','w')
            f.write("r2:"+str(r2)+"\n")
            f.write("RMSE:"+str(rmse)+"\n")
            f.write("MASE:"+str(mase_score)+"\n")
            f.close()
            y_max=y_train_predict
            save_model_high(path,lmodellineaire,sys.argv[3],r2,rmse,mase_score)
            
            
    writeelement(sys.argv[2],data[1]['x'],y_max,sys.argv[3],sys.argv[4])
    memoire=None
           
#
#
##Evaluation du training set
#y_train_predict = lmodellineaire.predict(X_train)
#rmse = (np.sqrt(mean_squared_error(Y_train, y_train_predict)))
#r2 = r2_score(Y_train, y_train_predict)
# 
#print('La performance du modèle sur la base dapprentissage')
#print('--------------------------------------')
#print('Lerreur quadratique moyenne est {}'.format(rmse))
#print('le score R2 est {}'.format(r2))
#print('\n')
# 
## model evaluation for testing set
#y_test_predict = lmodellineaire.predict(X_test)
#rmse = (np.sqrt(mean_squared_error(Y_test, y_test_predict)))
#r2 = r2_score(Y_test, y_test_predict)
# 
#print('La performance du modèle sur la base de test')
#print('--------------------------------------')
#print('Lerreur quadratique moyenne est {}'.format(rmse))
#print('le score R2 est {}'.format(r2))
