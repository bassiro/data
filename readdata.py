#!/usr/bin/env python
# coding: utf-8
from __future__ import (print_function, division,
    absolute_import, unicode_literals)
import sys
import warnings
import pandas as pd 
import numpy as np
import matplotlib
import os
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(0,os.getcwd()+'/src/')


from util import writeoutput
from util import writescoreall


def creatdataplot(path,dataplot,k,key):
    """"
    creat plot
    """
    ok=0
    #dataplot=None
    #dataplot="""data={1:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'test'},2:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLP'},3:{'x':['2019-01-01','2019-02-01','2019-03-01','2019-04-01'],'y': [1,2,3,4],'name':'MLPGO'}}"""
    try:
        f = open(path+'.tmp','w')
        if 'graph_final' in  path:
            f.write('data='+str(dataplot).replace("u'","'")[:-1])
        else:
            f.write('data='+str(dataplot).replace("u'","'"))    
        f.close()
        ok=1
    except:
        ok=0
        pass
    return ok

class Datalolad():
    def __init__(self,base_dir):        
        self.annuel_df=None
        self.annuel_df =None
        self.month_df =None
        self.logistics_df =None
        self.data_dictionary_df =None
        self.product_df=None 
        self.base_dir =base_dir#os.getcwd()+'/FinalData/'
        self.list_code_distrit=None
        self.list_code_region=None
        self.list_code_distict_ville=None
        self.list_code=None
 
    def read_data(self):
        self.annuel_df = pd.read_csv(os.path.join(self.base_dir, 'contraceptive_case_data_annual.csv'))
        self.month_df = pd.read_csv(os.path.join(self.base_dir, 'contraceptive_case_data_monthly.csv'))
        self.logistics_df = pd.read_csv(os.path.join(self.base_dir, 'contraceptive_logistics_data.csv'))
        self.data_dictionary_df = pd.read_csv(os.path.join(self.base_dir, 'data_dictionary.csv'))
        self.product_df = pd.read_csv(os.path.join(self.base_dir, 'product.csv'))
    
    def make_label_todata(self):
        for el in list(self.product_df.index):
            self.logistics_df['product_code']=self.logistics_df['product_code'].replace(self.product_df['product_code'][el],el)
        self.list_code=np.unique(self.logistics_df['site_code'])
        self.list_code_region=np.unique(self.logistics_df['region'])
        list_district=[]
        list_code_distict_ville0=list(np.unique(self.logistics_df['district']))
        list_code_distict_ville1=list(np.unique(self.annuel_df['district']))
        list_code_distict_ville2=list(np.unique(self.month_df['district']))
       
        for el in list_code_distict_ville0:
            if el not in list_district:
                list_district.append(el)
                
        for el in list_code_distict_ville1:
            if el not in list_district:
                list_district.append(el)
                
        for el in list_code_distict_ville2:
            if el not in list_district:
                list_district.append(el)
        
        for el3 in range(0,len(list_district)):
            self.logistics_df['district']=self.logistics_df['district'].replace(list_district[el3],el3)
            self.annuel_df['district']=self.annuel_df['district'].replace(list_district[el3],el3)
            self.month_df['district']=self.month_df['district'].replace(list_district[el3],el3)
       
        for el1 in range(0,len(self.list_code)):
            self.logistics_df['site_code']=self.logistics_df['site_code'].replace(self.list_code[el1],el1)
        
        for el2 in range(0,len(self.list_code_region)):
            self.logistics_df['region']=self.logistics_df['region'].replace(self.list_code_region[el2],el2)
        
        
            #annuel_df['district']=annuel_df['district'].replace(list_code_distict_ville[el3],el3)    
    def function_list_query(self,list_all_district):
        string='('
        for k in range(len(list_all_district)):
            if k<len(list_all_district)-1:
                string+='site_code=='+str(list_all_district[k])+' or '
            else:
                string+='site_code=='+str(list_all_district[k])+')'
        return string

    def get_current(self,nbr,key,columns_name,prod):        
        dat_c=None
        el=list(np.unique(self.logistics_df['site_code']))[key]
        d3f=self.logistics_df.query('site_code=='+str(el))    
        dist=list(np.unique(d3f['district']))[0]
        regio=list(np.unique(d3f['region']))[0]
        list_all_district=np.unique(self.logistics_df.query('district=='+str(dist))['site_code']) # same district
        list_all_region=np.unique(self.logistics_df.query('region=='+str(regio))['site_code'])      #same region 
        logistics_df=self.logistics_df.query(self.function_list_query(list_all_district))
        month_df=self.month_df.query('district=='+str(dist))  
        annuel_df=self.annuel_df.query('district=='+str(dist))  
        if nbr==1:
            cwd = os.getcwd()
            try:
                os.mkdir(cwd+'/'+str(key))
            except:
                pass
            for k in list(self.product_df.index):    
                f = open(str(key)+'/prod_'+str(k)+'.data_plot', "w")
                f.close()
                try:
                    lo=logistics_df.query('product_code=='+str(k)+'and site_code=='+str(el))
                    dat_c=[]
                    for ez in range(len(list(lo['year']))):
                        if len(str(list(lo['month'])[ez]))==1:
                            dt=str(list(lo['year'])[ez])+'-0'+str(list(lo['month'])[ez])
                        else:
                            dt=str(list(lo['year'])[ez])+'-'+str(list(lo['month'])[ez])
                        dat_c.append([dt,int(list(lo[columns_name])[ez])])
                    dat_c.sort()
                    f = open(str(key)+'/prod_'+str(k)+'.data_plot', "a")
                    for gb in dat_c:
                        f.write(str(gb[0])+'-01'+' '+str(gb[1])+'\n')
                    f.close()
                except:
                    pass
        if nbr==2:
           try:
              lo=logistics_df.query('product_code=='+str(prod)+'and site_code=='+str(el))
              dat_g=[]
              x=[]
              y=[]
              for ez in range(len(list(lo['year']))):
                  if len(str(list(lo['month'])[ez]))==1:
                     dt=str(list(lo['year'])[ez])+'-0'+str(list(lo['month'])[ez])
                  else:
                     dt=str(list(lo['year'])[ez])+'-'+str(list(lo['month'])[ez])
                  dat_g.append([dt,int(list(lo[columns_name])[ez])])
                  dat_g.sort()
              for el in range(len(dat_g)):
                  x.append(dat_g[el][0])
                  y.append(dat_g[el][1])
              dat_c=[x,y]
           except:
              pass                
        return annuel_df,month_df,logistics_df,dat_c
        
def load_init(cwd,data):
    """ 14 minutes processeurs I5 RAM 4GO """
    for distr in range(len(np.unique(data.logistics_df['site_code']))): #40range(130,150)
        annuel_df,month_df,logistics_df,dat_c=data.get_current(1,distr,u'stock_distributed',0)                  
        for prod in data.product_df.index:
            try:
               if sys.platform!='win32':
                  chemin1=os.getcwd()+'/model/'+str(distr)+'/'
               else:
                  chemin1=os.getcwd()+'\\model\\'+str(distr)+'\\'
               os.mkdir(chemin1)   
            except:
                pass 
            try:
               if sys.platform!='win32':
                  chemin=os.getcwd()+'/model/'+str(distr)+'/'+str(prod)+'/'
               else:
                  chemin=os.getcwd()+'\\model\\'+str(distr)+'\\'+str(prod)+'\\'
               os.mkdir(chemin)   
            except:
                pass
    
    for distr in range(len(np.unique(data.logistics_df['site_code']))): #40range(130,150)
        for prod in list(data.product_df.index):
            annuel_df,month_df,logistics_df,data_cal=data.get_current(2,distr,u'stock_distributed',prod)          
            dataplot={1:{'x':data_cal[0],'y': data_cal[1],'name':'real'}}
            path=cwd+'/'+str(distr)+'/graph_'+str(prod)
            ok=creatdataplot(path,dataplot,prod,distr)
            if ok==1:
                os.system('python write_graphe.py '+path)    
            os.system('rm -r '+path+'.tmp')



        
base_dir ='/home/t/Bureau/Competition/src/FinalData/'
data=Datalolad(base_dir)

data.read_data()
data.make_label_todata()


key=0

cwd = os.getcwd()

# init data 
#load_init(cwd,data)

####Liste des modele a mettre a jour a chaque declaration de models avec le nom du models 
list_model=['LSTM','MLP_1','lineare']
    
for distr in range(len(np.unique(data.logistics_df['site_code'])))[0:2]: #40range(130,150)
    for prod in list(data.product_df.index):
        annuel_df,month_df,logistics_df,data_cal=data.get_current(2,distr,u'stock_distributed',prod)          
        dataplot={1:{'x':data_cal[0],'y': data_cal[1],'name':'real'}}
        
        path=cwd+'/'+str(distr)+'/graph_'+str(prod)
        path2=cwd+'/'+str(distr)+'/graph_final_'+str(prod)
        path_out=cwd+'/'
        #creat data to plot all """
        #creatdataplot(path2,dataplot,prod,distr)
        
        #creat data to use simple model
        
        #ok=creatdataplot(path,dataplot,prod,distr)
        
        #print(path_out)
        
        ok=1
        if ok==1:
            g=os.system('python '+cwd+'/src/LSTM.py '+base_dir+' '+str(distr)+' '+str(prod)+' LSTM'+' 2')
        
        
        #permet d'ecrire le fichiers resultats

        
        """
        if ok==1:
            print('python '+cwd+'/src/lineare.py '+path+' '+path2+' lineare'+' 2')
            g=os.system('python '+cwd+'/src/lineare.py '+path+' '+path2+' lineare'+' 2')
        
        
        if ok==1:
            g=os.system('python '+cwd+'/src/SVR.py '+path+' '+path2+' SVR'+' 3')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_1'+' 4 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_2'+' 5 identity lbfgs 1e-7 70 30')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_3'+' 6 identity lbfgs 1e-7 20 1')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_4'+' 7 logistic lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_5'+' 8 logistic lbfgs 1e-7 70 30')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_6'+' 9 logistic lbfgs 1e-7 20 1')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_7'+' 10 tanh lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_8'+' 11 tanh lbfgs 1e-7 70 30')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_9'+' 12 tanh lbfgs 1e-7 20 1')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_10'+' 13 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_11'+' 14 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_12'+' 15 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_13'+' 16 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_14'+' 17 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_15'+' 18 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_16'+' 19 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_17'+' 20 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_18'+' 21 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_19'+' 22 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_20'+' 23 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_21'+' 24 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_22'+' 25 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_23'+' 26 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_24'+' 27 identity lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_25'+' 28 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_26'+' 29 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_27'+' 30 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_28'+' 31 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_29'+' 32 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_30'+' 33 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_31'+' 34 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_32'+' 35 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_33'+' 36 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_34'+' 37 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_35'+' 38 adam lbfgs 1e-7 100 10')        
        if ok==1:
            g=os.system('python '+cwd+'/src/MLP.py '+path+' '+path2+' MLP_36'+' 39 adam lbfgs 1e-7 100 10')        
        
        if ok==1:
            print('python write_graphe.py '+path2)
            os.system('python write_graphe.py '+path2)    
        """
        optimalmodel=writescoreall(path_out,distr,prod,list_model)
        writeoutput(path_out,distr,prod,optimalmodel)
        








    